﻿using UnityEngine;
using System.Collections;
using System;

public class Global : MonoBehaviour 
{
    private static Global instance = null;
    public static Global Instance { get { return instance; } }

    public Loading loading;

    public string levelName = "";

    public GameObject logText;

	private float timer;
	private bool allowClick = true;
	private bool startTimer = false;

    bool pause = false;

    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }
        else
            instance = this;
        DontDestroyOnLoad(this.gameObject);

    }
	void Start () 
    {
        StartCoroutine(loading.LoadScene("MainMenu", true));
	}
	public void InstantiateLogText(Vector3 position, string text)
    {
        GameObject go = Instantiate(logText, position, Quaternion.identity) as GameObject;
        go.GetComponent<TextMesh>().text = text;
    }
	public void ButtonEvent(Button.EventName name, string parameter)
    {
        if(allowClick)
		{

	        switch(name)
	        {
                case Button.EventName.ToMap:
                    StartCoroutine(loading.LoadScene("Map", false));
                    allowClick = false;
                    startTimer = true;
                    pause = false;
                    EventManager.Instance.OnPauseGame(pause);
	                break;
	            case Button.EventName.PlayLevel:
	                levelName = parameter;
                    if(int.Parse(levelName) % 2 == 0)
					{
						StartCoroutine(loading.LoadScene("WorldNight", false));
					}
					else
					{
		                StartCoroutine(loading.LoadScene("World", false));
					}
					allowClick = false;
					startTimer = true;
	                break;
	            case Button.EventName.ToMenu:
	                StartCoroutine(loading.LoadScene("MainMenu", false));
					allowClick = false;
					startTimer = true;
	                break;
                case Button.EventName.ToShop:
	                StartCoroutine(loading.LoadScene("Shop", false));
					allowClick = false;
					startTimer = true;
	                break;
	            case Button.EventName.ArcherLeft:
	                EventManager.Instance.OnArhcerChanged(-1);
	                break;
	            case Button.EventName.ArcherRight:
	                EventManager.Instance.OnArhcerChanged(1);
	                break;
	            case Button.EventName.CategoryLeft:
	                EventManager.Instance.OnCategoryChanged(-1);
	                break;
	            case Button.EventName.CategoryRight:
	                EventManager.Instance.OnCategoryChanged(1);
	                break;
	            case Button.EventName.Improve:
	                EventManager.Instance.OnStatisticChanged(Int32.Parse(parameter.Substring(parameter.IndexOf('_') + 1)));
	                break;
	            case Button.EventName.Buy:
	                EventManager.Instance.OnBuySetArcher();
	                break;
                case Button.EventName.LeaderBoard:
                    SocialManager.Instance.ShowLeaderBoards();
                    break;
                case Button.EventName.Achievements:
                    SocialManager.Instance.ShowAchievments();
                    break;
                case Button.EventName.Options:
                    MainMenuController.Instance.SetUiVisible(1);
                    break;
                case Button.EventName.About:
                    MainMenuController.Instance.SetUiVisible(2);
                    break;
                case Button.EventName.Back:
                    MainMenuController.Instance.SetUiVisible(0);
                    break;
                case Button.EventName.Music:
                    MusicManager.Instance.SwitchMusic(!MusicManager.Instance.IsEnabled);
                    break;
                case Button.EventName.Sound:
                    SoundManager.Instance.SwitchSound(!SoundManager.Instance.IsEnabled);
                    break;
                case Button.EventName.Pause:
                    pause = !pause;
                    EventManager.Instance.OnPauseGame(pause);
                    break;

	        }
		}
    }
	void Update () 
    {
		if(startTimer)
		{
			timer += Time.deltaTime;
			if(timer >= 1.0f)
			{
				startTimer = false;
				timer = 0f;
				allowClick = true;
			}
		}
	}
}
