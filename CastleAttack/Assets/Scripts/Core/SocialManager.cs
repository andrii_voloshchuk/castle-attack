﻿using UnityEngine;
using System.Collections;
using GooglePlayGames;

public class SocialManager : MonoBehaviour 
{
    private static SocialManager instance = null;
    public static SocialManager Instance { get { return instance; } }



    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }
        else
            instance = this;
        DontDestroyOnLoad(this.gameObject);

    }

	void Start () 
    {
        PlayGamesPlatform.Activate();
        Social.Active.localUser.Authenticate((bool success) =>
            {
                if (success)
                {

                }
            });
	}
	
    public void ShowLeaderBoards()
    {
        if (Social.Active.localUser.authenticated == true)
        {
            PlayGamesPlatform.Instance.ShowLeaderboardUI("CggIyuvwll4QAhAB");
        }
        else
        {
            Social.Active.localUser.Authenticate((bool success) =>
                {
                    if (success)
                    {
                        PlayGamesPlatform.Instance.ShowLeaderboardUI("CggIyuvwll4QAhAB");
                    }
                });
        }
    }
    public void ReportScore(int score)
    {
        if (Social.Active.localUser.authenticated == true)
        {
            Social.Active.ReportScore(score, "CggIyuvwll4QAhAB", (bool success) =>
                {

                });
        }
    }
    public void ReportAchievement(string id, int steps)
    {
        if (Social.Active.localUser.authenticated == true)
        {
            PlayGamesPlatform.Instance.IncrementAchievement(id, steps, (bool success) =>
                {
                });
        }
    }
    public void ReportAchievement(string id)
    {
        if (Social.Active.localUser.authenticated == true)
        {
            PlayGamesPlatform.Instance.ReportProgress(id, 100.0, (bool success) =>
                {
                });
        }
    }
    public void ShowAchievments()
    {
        if (Social.Active.localUser.authenticated == true)
        {
            PlayGamesPlatform.Instance.ShowAchievementsUI();
        }
        else
        {
            Social.Active.localUser.Authenticate((bool success) =>
                {
                    if (success)
                    {
                        PlayGamesPlatform.Instance.ShowAchievementsUI();
                    }
                });
        }
    }
}
