﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MusicManager : MonoBehaviour {

    public List<AudioClip> music;

    private static MusicManager instance = null;
    public static MusicManager Instance { get { return instance; } }

    private AudioSource audioSource;

    private bool isEnabled = false;

    public bool IsEnabled { get{ return isEnabled; } set { isEnabled = value; } }
    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }
        else
            instance = this;
        DontDestroyOnLoad(this.gameObject);
        audioSource = GetComponent<AudioSource>();

    }
    void Start () 
    {
        audioSource.loop = true;
        PlayOneClip("MainMenu");
    }
    public void SwitchMusic(bool value)
    {
        isEnabled = value;
        audioSource.mute = !value;
        PlayerPrefs.SetString ("music", isEnabled.ToString());
        PlayerPrefs.Save ();
    }

    private AudioClip GetClipByName(string clipName)
    {
        foreach(AudioClip clip in music)
        {
            if (clip.name == clipName)
                return clip;
        }
        return music[0];
    }
    public void PlayRandomClip(string name, int min, int max)
    {
        int random = Random.Range (min, max + 1);
        audioSource.clip = GetClipByName (name + "_" + random);
        audioSource.Play();
    }
    public void PlayOneClip(string name)
    {
        if (audioSource.clip == null || (audioSource.clip != null && audioSource.clip.name != name))
        {
            audioSource.Stop();
            audioSource.clip = GetClipByName(name);
            audioSource.Play();
        }
    }
    public void Stop()
    {
        audioSource.Stop();
    }
}
