﻿using UnityEngine;
using System.Collections;

public class EventManager : MonoBehaviour
{
    private static EventManager instance = null;
    public static EventManager Instance { get { return instance; } }
   
    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }
        else
            instance = this;
        DontDestroyOnLoad(this.gameObject);

    }
    public delegate void OnGameOverEventHandler();
    public static event OnGameOverEventHandler onGameOver;
    public void OnGameOver()
    {
        if (onGameOver != null)
        {
            onGameOver();
        }
    }

    public delegate void OnPointsChangedEventHandler(int points);
    public static event OnPointsChangedEventHandler onPointsChanged;
    public void OnPointsChanged(int points)
    {
        if(onPointsChanged != null)
        {
            onPointsChanged(points);
        }
    }
    public delegate void OnArhcerChangedEventHandler(int step);
    public static event OnArhcerChangedEventHandler onArhcerChanged;
    public void OnArhcerChanged(int step)
    {
        if (onArhcerChanged != null)
        {
            onArhcerChanged(step);
        }
    }
    public delegate void OnCategoryChangedEventHandler(int step);
    public static event OnCategoryChangedEventHandler onCategoryChanged;
    public void OnCategoryChanged(int step)
    {
        if (onCategoryChanged != null)
        {
            onCategoryChanged(step);
        }
    }
    public delegate void OnStatisticChangedEventHandler(int index);
    public static event OnStatisticChangedEventHandler onStatisticChanged;
    public void OnStatisticChanged(int index)
    {
        if (onStatisticChanged != null)
        {
            onStatisticChanged(index);
        }
    }
    public delegate void OnBuySetArcherEventHandler();
    public static event OnBuySetArcherEventHandler onBuySetArcher;
    public void OnBuySetArcher()
    {
        if (onBuySetArcher != null)
        {
            onBuySetArcher();
        }
    }
    public delegate void OnPauseGameEventHandler(bool value);
    public static event OnPauseGameEventHandler onPauseGame;
    public void OnPauseGame(bool value)
    {
        if (onPauseGame != null)
        {
            onPauseGame(value);
        }
    }
}
