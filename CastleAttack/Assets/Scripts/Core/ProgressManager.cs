﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;

public class ProgressManager : MonoBehaviour
{
    private static ProgressManager instance = null;
    public static ProgressManager Instance { get { return instance; } }

    public List<Level> levels;

    private string skey = "castleattackpracadyplomowa2015andriivoloshchuk";

    public int gold = 0;

    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }
        else
            instance = this;
        DontDestroyOnLoad(this.gameObject);

        InitOptions();
        InitGold ();
        InitProgress();
    }

    private void InitGold()
    {
        if(SecurePlayerPrefs.HasKey("Gold"))
        {
            gold = int.Parse(SecurePlayerPrefs.GetString("Gold", skey));
        }
        else
        {
            gold = 100;
            SecurePlayerPrefs.SetString("Gold", gold.ToString(), skey);
            PlayerPrefs.Save();
        }
    }
    private void InitOptions()
    {
        bool sound = true;
        bool music = true;
        if (PlayerPrefs.HasKey("sound"))
        {
            if (PlayerPrefs.GetString("sound").Contains("False"))
                sound = false;
        }
        else
        {
            PlayerPrefs.SetString("sound", "True");
        }
        if (PlayerPrefs.HasKey("music"))
        {
            if (PlayerPrefs.GetString("music").Contains("False"))
                music = false;
        }
        else
        {
            PlayerPrefs.SetString("music", "True");
        }
        SoundManager.Instance.SwitchSound(sound);
        MusicManager.Instance.SwitchMusic(music);
    }
	public string GetData()
    {
        if(SecurePlayerPrefs.HasKey("Data"))
        {
            return SecurePlayerPrefs.GetString("Data", skey);
        }
        return "";
    }
    public void SaveData(string data)
    {
        SecurePlayerPrefs.SetString("Data", data, skey);
        PlayerPrefs.Save();
    }
    public void AddGold(int value)
    {
        gold += value;
        EventManager.Instance.OnPointsChanged(value);
        SecurePlayerPrefs.SetString("Gold", gold.ToString(), skey);
        PlayerPrefs.Save();
    }
    private void InitProgress()
    {
        levels = new List<Level>();
        if(SecurePlayerPrefs.HasKey("Progress"))
        {
            var node = JSON.Parse(SecurePlayerPrefs.GetString("Progress", skey));
            for (int i = 0; i < node["levels"].Count; i++)
            {
                levels.Add(new Level(node["levels"][i]["name"].AsInt, node["levels"][i]["state"].AsInt, node["levels"][i]["score"].AsInt));
            }
        }
        else
        {
            TextAsset progress = Resources.Load("Progress") as TextAsset;
            var node = JSON.Parse(progress.text);
            for (int i = 0; i < node["levels"].Count; i++)
            {
                levels.Add(new Level(node["levels"][i]["name"].AsInt, node["levels"][i]["state"].AsInt, node["levels"][i]["score"].AsInt));
            }
            SaveProgress();
        }
        
    }

    public void SaveProgress()
    {
        JSONClass json = new JSONClass();
        for(int i = 0; i < levels.Count; i++)
        {
            json["levels"][i]["name"] = levels[i].name.ToString();
            json["levels"][i]["state"] = levels[i].state.ToString();
            json["levels"][i]["score"] = levels[i].score.ToString();
        }
        SecurePlayerPrefs.SetString("Progress", json.ToString(), skey);
        PlayerPrefs.Save();
    }

    public void UpdateLevel(int name, int score)
    {
        for(int i = 0; i < levels.Count; i++)
        {
            if(levels[i].name == name)
            {
                if(levels[i].score < score)
                    levels[i].score = score;
                if (levels[i].state < 2)
                {
                    levels[i].state = 2;
                }
                if (i + 1 < levels.Count)
                {
                    if (levels[i + 1].state < 1)
                    {
                        levels[i + 1].state = 1;
                    }
                }
                SaveProgress();
                return;
            }
        }
    }

	
	void Update ()
    {
	    if(Input.GetKeyDown(KeyCode.G))
        {
            AddGold(1000);
        }
		if(Input.GetKeyDown(KeyCode.D))
		{
			PlayerPrefs.DeleteAll();
		}
	}
}
public class Level
{
    public int name;
    public int state;
    public int score;

    public Level(int name, int state, int score)
    {
        this.name = name;
        this.state = state;
        this.score = score;
    }
}
