﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SoundManager : MonoBehaviour 
{
    public List<AudioClip> sounds;

    private static SoundManager instance = null;
    public static SoundManager Instance { get { return instance; } }

    private AudioSource audioSource;

    private bool isEnabled = false;
    public bool IsEnabled { get{ return isEnabled; } set { isEnabled = value; } }

    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }
        else
            instance = this;
        DontDestroyOnLoad(this.gameObject);

    }
	void Start () 
    {
        audioSource = GetComponent<AudioSource>();
	}
    public void SwitchSound(bool value)
    {
        isEnabled = value;
        PlayerPrefs.SetString ("sound", isEnabled.ToString());
        PlayerPrefs.Save ();
    }
	
    private AudioClip GetClipByName(string clipName)
    {
        foreach(AudioClip clip in sounds)
        {
            if (clip.name == clipName)
                return clip;
        }
        return sounds[0];
    }
    public void PlayRandomSound(string name, int min, int max)
    {
        if (isEnabled) 
        {
            int random = Random.Range (min, max + 1);
            audioSource.PlayOneShot (GetClipByName (name + "_" + random));
        }
    }
    public void PlayOneSound(string name)
    {
        if(isEnabled)
            audioSource.PlayOneShot(GetClipByName(name));
    }
    public void Play(string clipName)
    {
        if (isEnabled)
        {
            audioSource.clip = GetClipByName(clipName);
            audioSource.Play();
        }
    }
    public void Stop(string clipName)
    {
        if (audioSource.clip != null)
        {
            if (audioSource.clip.name == clipName)
                audioSource.clip = null;
        }
    }
}
