﻿using UnityEngine;
using System.Collections;
using SimpleJSON;
using System.Collections.Generic;

public class ShopManager : MonoBehaviour 
{
    public List<GameObject> categories;
    public TextMesh categoryName;

    public TextMesh archerName;
    public SpriteRenderer archer;

    public TextMesh buyButton;

    public List<TextMesh> costs;
    public List<Statistic> statistics;
    public List<Sprite> sprites;
    public List<GameObject> buttons;


    private List<ArcherInfo> archers = new List<ArcherInfo>();


    private int currentArcher = 0;
    private int currentCategory = 0;
    private int wallDefence = 0;
    private int wallHeight = 0;
    private int setArcher = 0;

	void Start () 
    {
        EventManager.onArhcerChanged += EventManager_onArhcerChanged;
        EventManager.onStatisticChanged += EventManager_onStatisticChanged;
        EventManager.onCategoryChanged += EventManager_onCategoryChanged;
        EventManager.onBuySetArcher += EventManager_onBuySetArcher;
        LoadData();
	}

    
    void OnDestroy() 
    {
        EventManager.onArhcerChanged -= EventManager_onArhcerChanged;
        EventManager.onStatisticChanged -= EventManager_onStatisticChanged;
        EventManager.onCategoryChanged -= EventManager_onCategoryChanged;
        EventManager.onBuySetArcher -= EventManager_onBuySetArcher;
    }
    void EventManager_onBuySetArcher()
    {
		if(currentCategory == 0)
		{
	        if(archers[currentArcher].state == 1)
	        {        
	            setArcher = currentArcher;
				Global.Instance.InstantiateLogText(new Vector3(0, 3.25f, -5f),  archers[currentArcher].name + " is choosen!");

	        }
	        else
	        {
	            if (ProgressManager.Instance.gold >= currentArcher * 5000)
	            {
	                ProgressManager.Instance.AddGold(-(currentArcher * 5000));
	                archers[currentArcher].state = 1;
	                ChangeArcher();
	            }
	            else
	            {
	                Global.Instance.InstantiateLogText(new Vector3(0, 3.25f, -5f), "You don't have enough money...");
	            }
	        }
		}
		
        SaveData();

    } 
    void EventManager_onCategoryChanged(int step)
    {
        if (step < 0 && currentCategory > 0)
        {
            currentCategory += step;
            ChangeCategory();
        }
        else if (step > 0 && currentCategory < 1)
        {
            currentCategory += step;
            ChangeCategory();
        }
    }
    void EventManager_onArhcerChanged(int step)
    {
		if(currentCategory == 0)
		{
	        if (step < 0 && currentArcher > 0)
	        {
	            currentArcher += step;
	            ResetStatistics();
	            ChangeArcher();
	        }
	        else if(step > 0 && currentArcher < 2)
	        {
	            currentArcher += step;
	            ResetStatistics();
	            ChangeArcher();
	        }
		}

    }
    void EventManager_onStatisticChanged(int index)
    {
        switch(index)
        {
            case 0:
                if (archers[currentArcher].armor < 10)
                {
                    if (ProgressManager.Instance.gold >= archers[currentArcher].armor * 100 + 100)
                    {
                        ProgressManager.Instance.AddGold(-(archers[currentArcher].armor * 100 + 100));
                        archers[currentArcher].armor++;
                    }
                    else
                    {
                        Global.Instance.InstantiateLogText(new Vector3(0, 3.25f, -5f), "You don't have enough money...");
                    }
                }
				else
				{
					Global.Instance.InstantiateLogText(new Vector3(0, 3.25f, -5f), "Maximum value reached");
				}
                break;
            case 1:
                if (archers[currentArcher].damage < 10)
                {
                    if (ProgressManager.Instance.gold >= archers[currentArcher].damage * 100 + 100)
                    {
                        ProgressManager.Instance.AddGold(-(archers[currentArcher].damage * 100 + 100));
                        archers[currentArcher].damage++;
                    }
                    else
                    {
                        Global.Instance.InstantiateLogText(new Vector3(0, 3.25f, -5f), "You don't have enough money...");
                    }
                }
				else
				{
					Global.Instance.InstantiateLogText(new Vector3(0, 3.25f, -5f), "Maximum value reached");
				}
                break;
            case 2:
                if (archers[currentArcher].accuracy < 10)
                {
                    if (ProgressManager.Instance.gold >= archers[currentArcher].accuracy * 100 + 100)
                    {
                        ProgressManager.Instance.AddGold(-(archers[currentArcher].accuracy * 100 + 100));
                        archers[currentArcher].accuracy++;
                    }
                    else
                    {
                        Global.Instance.InstantiateLogText(new Vector3(0, 3.25f, -5f), "You don't have enough money...");
                    }
                }
				else
				{
					Global.Instance.InstantiateLogText(new Vector3(0, 3.25f, -5f), "Maximum value reached");
				}
                break;
            case 3:
                if (wallHeight < 10)
                {
                    if (ProgressManager.Instance.gold >= wallHeight * 100 + 100)
                    {
                        ProgressManager.Instance.AddGold(-(wallHeight * 100 + 100));
                        wallHeight++;
                    }
                    else
                    {
                        Global.Instance.InstantiateLogText(new Vector3(0, 3.25f, -5f), "You don't have enough money...");
                    }
                }
				else
				{
					Global.Instance.InstantiateLogText(new Vector3(0, 3.25f, -5f), "Maximum value reached");
				}
                break;
            case 4:
                if(wallDefence < 10)
                {
                    if (ProgressManager.Instance.gold >= wallDefence * 100 + 100)
                    {
                        ProgressManager.Instance.AddGold(-(wallDefence * 100 + 100));
                        wallDefence++;
                    }
                    else
                    {
                        Global.Instance.InstantiateLogText(new Vector3(0, 3.25f, -5f), "You don't have enough money...");
                    }
                }
				else
				{
					Global.Instance.InstantiateLogText(new Vector3(0, 3.25f, -5f), "Maximum value reached");
				}
                break;
           
        }
        if (index < 3)
        {
            ChangeArcher();
        }
        else
        {
            ChangeWall();
        }
        SaveData();

    }
	private void LoadData()
    {
        if(SecurePlayerPrefs.HasKey("Data"))
        {
            JSONNode node = JSON.Parse(ProgressManager.Instance.GetData());
            ParseData(node);
        }
        else
        {
            TextAsset text = Resources.Load("Data") as TextAsset;
            JSONNode node = JSON.Parse(text.text);
            ProgressManager.Instance.SaveData(text.text);
            ParseData(node);
        }
    }
    private void ResetStatistics()
    {
        for (int i = 0; i < statistics.Count; i++)
        {
            statistics[i].Reset();
        }
    }
    private void ParseData(JSONNode node)
    {
        for(int i = 0; i < node["archers"].Count; i++)
        {
            archers.Add(new ArcherInfo(node["archers"][i]["name"],
                node["archers"][i]["armor"].AsInt,
                node["archers"][i]["damage"].AsInt, 
                node["archers"][i]["accuracy"].AsInt,
                node["archers"][i]["state"].AsInt));
        }

        currentArcher = node["archer"].AsInt;
        wallHeight = node["height"].AsInt;
        wallDefence = node["defence"].AsInt;
		setArcher = currentArcher;
        ChangeArcher();

    }
    private void ChangeWall()
    {
        statistics[3].SetStatstic(wallHeight * 10f, 100f);
        statistics[4].SetStatstic(wallDefence * 10f, 100f);

        costs[3].text = (wallHeight * 100 + 100) + "";
        costs[4].text = (wallDefence * 100 + 100) + "";
    }
	private void ChangeArcher()
    {
        archerName.text = archers[currentArcher].name;
        archer.sprite = sprites[currentArcher];
        if(archers[currentArcher].state == 0)
        {
            foreach(GameObject go in buttons)
            {
                go.collider2D.enabled = false;
                go.GetComponent<SpriteRenderer>().color = new Color(0.5f, 0.5f, 0.5f);
            }
            archer.color = new Color(0.05f, 0.05f, 0.05f);
            buyButton.text = currentArcher * 5000 + "";
        }
        else
        {
            foreach (GameObject go in buttons)
            {
                go.collider2D.enabled = true;
                go.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f);
            }
            archer.color = new Color(1f, 1f, 1f);
            buyButton.text = "set";

        }
        statistics[0].SetStatstic(archers[currentArcher].armor * 10f, 100f);
        statistics[1].SetStatstic(archers[currentArcher].damage * 10f, 100f);
        statistics[2].SetStatstic(archers[currentArcher].accuracy * 10f, 100f);

        costs[0].text = (archers[currentArcher].armor * 100 + 100) + "";
        costs[1].text = (archers[currentArcher].damage * 100 + 100) + "";
        costs[2].text = (archers[currentArcher].accuracy * 100 + 100) + "";
    }
    private void ChangeCategory()
    {
        ResetStatistics();
        string cname = "";
        switch(currentCategory)
        {
            case 0: cname = "Archers"; ChangeArcher(); break;
            case 1: cname = "Walls"; ChangeWall(); break;
        }
        categoryName.text = cname;
        for(int i = 0; i < categories.Count; i++)
        {
            if(i != currentCategory)
            {
                categories[i].SetActive(false);
            }
            else
            {
                categories[i].SetActive(true);
            }
        }
    }

    private void SaveData()
    {
        JSONClass json = new JSONClass();
        for (int i = 0; i < archers.Count; i++)
        {
            json["archers"][i]["name"] = archers[i].name.ToString();
            json["archers"][i]["armor"] = archers[i].armor.ToString();
            json["archers"][i]["damage"] = archers[i].damage.ToString();
            json["archers"][i]["accuracy"] = archers[i].accuracy.ToString();
            json["archers"][i]["state"] = archers[i].state.ToString();
        }

        json["archer"] = setArcher.ToString();
        json["height"] = wallHeight.ToString();
        json["defence"] = wallDefence.ToString();
        ProgressManager.Instance.SaveData(json.ToString());  

    }
}
public class ArcherInfo
{
    public string name;
    public int armor;
    public int damage;
    public int accuracy;
    public string description;
    public int state;

    public ArcherInfo(string name, int armor, int damage, int accuracy, int state)
    {
        this.name = name;
        this.armor = armor;
        this.damage = damage;
        this.accuracy = accuracy;
        this.state = state;
    }
}
public class Equipment
{
	public string name;
	public int price;
	public int state;
	public string description;
	public Equipment(string name, int price, int state)
	{
		this.name = name;
		this.price = price;
		this.state = state;
	}
}
