﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;

public class GamePlay : MonoBehaviour 
{
    public List<GameObject> enemies;
	public List<Transform> walls;
    private List<Archer> archers = new List<Archer>();

	public List<GameObject> archersPrefabs;

    private TextAsset level;

    private List<Wave> waves;

    private float timeBetweenWaves = 0f;
    private float timeBetweenSpawnes = 0f;

    private float timerWaves = 0f;
    private float timerSpawnes = 0f;

    private int wave = 0;
    private int counter = 0;

    private bool allowSpawnWave = true;
    private bool allowSpawnUnit = false;

    private bool win = false;
    private bool gameOver = false;
    private bool nightMode = false;

    public Transform spawner;

    public GameObject winScreen;
    public GameObject gameOverScreen;
    public GameObject pauseScreen;

    private int levelName = 0;


    public TextMesh summaryText;
    public TextMesh summaryPointsText;

    public Animator lightning;
    private float lightningTimer = 0f;
    private float lightningTime = 0f;

	private bool initialized = false;

    public SpriteRenderer bar;

    bool isArcher = false;
	void Start () 
    {
        EventManager.onGameOver += EventManager_onGameOver;
        EventManager.onPauseGame += HandleonPauseGame;
        MusicManager.Instance.PlayRandomClip("Battle", 1, 3);
        levelName = System.Convert.ToInt32(Global.Instance.levelName);
        timeBetweenWaves = 1f;
        timeBetweenSpawnes = Random.Range(1f, 2f);
        waves = new List<Wave>();
        level = Resources.Load("Levels/" + levelName) as TextAsset;
        var node = JSON.Parse(level.text);
        if(levelName % 2 == 0)
        {
            lightningTime = Random.Range(5f, 20f);
            nightMode = true;
            PointsCounter.Instance.nightMode = true;
        }
        for(int i = 0; i < node["waves"].Count; i++)
        {
            waves.Add(new Wave(node["waves"][i]["type"], node["waves"][i]["amount"].AsInt));
        }
		SetArchers();
		initialized = true;
    }

    void HandleonPauseGame (bool value)
    {
        pauseScreen.SetActive(value);
        if (!value)
            Time.timeScale = 1;
        else
            Time.timeScale = 0;
    }
	private void SetArchers()
	{
		JSONNode node;
		if(SecurePlayerPrefs.HasKey("Data"))
		{
			node = JSON.Parse(ProgressManager.Instance.GetData());
		}
		else 
		{
			TextAsset data = Resources.Load("Data") as TextAsset;
			node = JSON.Parse(data.text);
		}
		int currentArcher = node["archer"].AsInt;
		float height = node["height"].AsFloat;
		int defence = node["defence"].AsInt;
        bool hasBraizer = PointsCounter.Instance.nightMode;

		defence = defence * 10;
		height = height / 10f;
		int armor = node["archers"][currentArcher]["armor"].AsInt;
		armor = armor * 10;
		float damage = node["archers"][currentArcher]["damage"].AsFloat;
		damage = damage * 2f;

		int accuracy = node["archers"][currentArcher]["accuracy"].AsInt;
		for(int i = 0; i < walls.Count; i++)
		{
			walls[i].localPosition = new Vector3(walls[i].localPosition.x, walls[i].localPosition.y + height, 0); 
			walls[i].GetComponent<Wall>().strength += defence;
			walls[i].GetComponent<Wall>().SetBraizer(hasBraizer);
			GameObject go = Instantiate(archersPrefabs[currentArcher], Vector3.zero, Quaternion.identity) as GameObject;
			go.transform.parent = walls[i];
			walls[i].GetComponent<Wall>().archer = go.GetComponent<Archer>();
			go.transform.localScale = new Vector3(1f,1f,1f);
			go.transform.localPosition = new Vector3(0, 0.15f, -1f);
			archers.Add(go.GetComponent<Archer>());
		}
		for (int i = 0; i < archers.Count; i++) 
		{
			archers[i].accuracy -= accuracy;
			archers[i].health += armor;
			archers[i].damage += damage;
			archers[i].hasBraizer = hasBraizer;
		}
	}
    void OnDestroy()
    {
        EventManager.onGameOver -= EventManager_onGameOver;
        EventManager.onPauseGame -= HandleonPauseGame;
    }
    void EventManager_onGameOver()
    {
        StartCoroutine(GameOver());
    }
	private GameObject GetEnemyByName(string type)
    {
        foreach(GameObject go in enemies)
        {
            if (go.name == type)
                return go;
        }
        return enemies[0];
    }
    private void Lightning()
    {
        lightningTimer += Time.deltaTime;
        if(lightningTimer >= lightningTime)
        {
            lightningTimer = 0f;
            lightningTime = Random.Range(5f, 20f);
            lightning.SetTrigger("Start");
            SoundManager.Instance.PlayOneSound("thunder");
        }
    }
	void Update () 
    {

		if(initialized)
		{
	        if (!win && !gameOver)
	        {
	            if (Input.GetMouseButtonDown(0))
	            {
                    float y = Camera.main.ScreenToWorldPoint(Input.mousePosition).y;
                    if (y <= bar.bounds.min.y)
                    {
                        float distance = Camera.main.ScreenToWorldPoint(Input.mousePosition).x;
                        bool wasShoot = false;
                        foreach (Archer archer in archers)
                        {
                            if (!archer.isLoading && !archer.isDead)
                            {
                                wasShoot = true;
                                if (!isArcher)
                                {
                                    if (archer && archer.gameObject.name.StartsWith("Archer"))
                                        isArcher = true;
                                }
                                float angle = (distance / 18f) * 170f;
                                StartCoroutine(archer.Shoot(angle));
                            }
                        }
                        if (wasShoot)
                        {

                            if(isArcher)
                                SoundManager.Instance.PlayRandomSound("arrow_shoot", 1, 3);
                            else
                                SoundManager.Instance.PlayRandomSound("cbow", 1, 3);

                        }
                    }
	            }
	            if(nightMode)
	            {
	                Lightning();
	            }
	            SpawnWaves();
	        }
		}
       
	}
    private void SpawnWaves()
    {
        if (wave < waves.Count)
        {
            if(allowSpawnWave)
            {
                timerWaves += Time.deltaTime;
                if (timerWaves >= timeBetweenWaves)
                {
                    allowSpawnWave = false;
                    timeBetweenWaves = Random.Range(1f, 4f);
                    timerWaves = 0f;
                }
            }
        }
        else
        {
            if(!EnemiesExist())
            {
                win = true;
                StartCoroutine(Win());
            }
        }
        
        if(!allowSpawnWave)
        {
            if(counter < waves[wave].amount)
            {
                timerSpawnes += Time.deltaTime;
                if (timerSpawnes >= timeBetweenSpawnes)
                {
                    allowSpawnUnit = true;
                    timerSpawnes = 0f;
                    timeBetweenSpawnes = Random.Range(0.5f, 2f);
                    counter++;
                }
            }
            else
            {
                counter = 0;
                wave ++;
                allowSpawnWave = true;
            }
            
        }
        if(allowSpawnUnit)
        {
             Instantiate(GetEnemyByName(waves[wave].type), spawner.position, Quaternion.identity);
             allowSpawnUnit = false;
        }                          
    }
    private bool EnemiesExist()
    {
        if (GameObject.FindGameObjectsWithTag("Enemy").Length > 0)
            return true;
        return false;
    }
    private IEnumerator Win()
    {
        yield return new WaitForSeconds(1f);
        MusicManager.Instance.Stop();
        SoundManager.Instance.Play("victory");
        winScreen.SetActive(true);
        KillAll();
        int points = PointsCounter.Instance.GetTotalPoints();
        string summaryPoints = PointsCounter.Instance.enemyKills +
            "\n" + PointsCounter.Instance.accuracy +
            "\n" + PointsCounter.Instance.survived +
            "\n" + PointsCounter.Instance.defence;
        string summary = "Enemies destroyed: " + 
            "\nShooting accuracy: " +
            "\nArchers survived: " +
            "\nWalls defence: ";
        if(PointsCounter.Instance.noHitBonus)
        {
            summary += "\nNo hit bonus: ";
            summaryPoints += "\n50";
        }
        summary += "\n\nTotal points: ";
        summaryPoints += "\n\n" + points; 
        summaryText.text = summary;
        summaryPointsText.text = summaryPoints;
		ProgressManager.Instance.AddGold(PointsCounter.Instance.enemyKills);
		ProgressManager.Instance.UpdateLevel(levelName, points);


    }
    public IEnumerator GameOver()
    {
        gameOver = true;
        yield return new WaitForSeconds(1f);
        gameOverScreen.SetActive(true);
        KillAll();
    }
    void KillAll()
    {
        foreach (GameObject go in GameObject.FindGameObjectsWithTag("Enemy"))
        {
            Destroy(go);
        }

    }
}
public class Wave
{
    public string type;
    public int amount;

    public Wave(string type, int amount)
    {
        this.type = type;
        this.amount = amount;
    }
}
