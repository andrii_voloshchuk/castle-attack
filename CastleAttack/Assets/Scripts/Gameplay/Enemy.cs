﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour
{
    public enum EnemyType
    {
        Infantry, Assasin, Archer, Slave
    }
    public EnemyType enemyType;

    public float speed = 0f;
    public float health = 0f;
    public float damageTime = 1f;
    public float damage = 0f;
    public int points = 0;
    private float timer = 0f;

    public bool isDead = false;
    private bool wallFound = false;
    public bool enemyFound = false;
    public bool idle = false;
    public HealthBar healthBar;

    public Animator animator;

    private Wall wall = null;

    private int idleCounter = 0;
    private float idleTimer = 0f;
    private float idleTime = 1f;

    public GameObject arrow;

    public Material nightMaterial;

	void Start () 
    {
        int level = int.Parse(Global.Instance.levelName);
        health += level;
        speed += level / 60f;
        damage += level / 10f;
        if (enemyType == EnemyType.Assasin)
        {
            collider2D.enabled = false;
        }
        else if (enemyType == EnemyType.Slave)
        {
            arrow.SetActive(false);
        }
        if(PointsCounter.Instance.nightMode)
        {
            if (enemyType == EnemyType.Slave)
            {
                arrow.SetActive(true);
            }
            renderer.material = nightMaterial;
            healthBar.renderer.material = nightMaterial;
        }
        animator = GetComponent<Animator>();
        healthBar.InitHealth(health);
        speed += Random.Range(-(speed / 10f), speed / 10f);
        
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.tag == "Wall")
        {
            wallFound = true;
            wall = other.GetComponent<Wall>();
            animator.SetBool("Fight", true);
        }
        if (other.gameObject.name == "GOTrigger")
        {
            EventManager.Instance.OnGameOver();
        }
    }
    void OnTriggerExit2D(Collider2D other)
    {
        if(other.gameObject.tag == "Wall")
        {
            wallFound = false;
            wall = null;
            animator.SetBool("Fight", false);
        }
    }
    public void Spoted()
    {
        if (enemyType == EnemyType.Assasin)
        {
            animator.SetTrigger("Spoted");
            collider2D.enabled = true;
        }
        else if (enemyType == EnemyType.Archer)
        {
            animator.SetBool("Shoot", true);
            enemyFound = true;
            idle = false;
            idleTime = 0f;
            idleCounter = 0;
        }
    }
    public void Damage(float damage)
    {
        healthBar.UpdateHealth(damage);
        float newHealth = health - damage;
        if(newHealth <= 0f)
        {
            health = 0f;
            isDead = true;
            animator.SetTrigger("Die");   
            if (gameObject.name.StartsWith("Lord"))
                SoundManager.Instance.PlayOneSound("lorddie");
            else
                SoundManager.Instance.PlayRandomSound("arrow_death", 1, 5);
            EventManager.Instance.OnPointsChanged(points);
            PointsCounter.Instance.enemyKilled++;
            StartCoroutine(Kill());
        }
        else
        {
            health = newHealth;
            SoundManager.Instance.PlayRandomSound("hit", 1, 5);
        }
    }
    private IEnumerator Kill()
    {
        yield return new WaitForSeconds(1.2f);
        transform.position = new Vector3(0, -50, 0);
        yield return new WaitForSeconds(0.1f);
        Destroy(this.gameObject);
    }
	void Update ()
    {
        if (enemyType == EnemyType.Archer)
        {
            if (!isDead)
            {
                if (!enemyFound && !idle)
                {
                    transform.Translate(Vector3.left * speed * Time.deltaTime);
                }
                else if(enemyFound)
                {
                    timer += Time.deltaTime;
                    if (timer >= damageTime)
                    {
                        timer = 0;
                        float angle = Random.Range(-270f, -200f);
                        Instantiate(arrow, transform.position, Quaternion.Euler(0, 0, angle));
                        SoundManager.Instance.PlayRandomSound("arrow_shoot", 1, 3);
                    }
                }
                else if (idle)
                {
                    idleTimer += Time.deltaTime;
                    if(idleTimer >= idleTime)
                    {
                        idleTimer = 0f;
                        idleTime = Random.Range(5f, 10f);
                        idleCounter++;
                        if(idleCounter > 4)
                        {
                            idleCounter = 1;
                        }
                        animator.SetInteger("Idle", idleCounter);
                    }
                }
            }
        }
        else
        {
            if(!isDead)
            {
                if (!wallFound)
                {
                    transform.Translate(Vector3.left * speed * Time.deltaTime);
                }
                else
                {
                    timer += Time.deltaTime;
                    if (timer >= damageTime)
                    {
                        timer = 0;
                        if (wall)
                            wall.Damage(damage);
                        SoundManager.Instance.PlayRandomSound("metrock", 1, 5);
                    }
                }
            }
        }
	}
}
