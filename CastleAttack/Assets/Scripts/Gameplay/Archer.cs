﻿using UnityEngine;
using System.Collections;

public class Archer : MonoBehaviour 
{
    public GameObject[] arrows;
    private Animator animator;
    public bool isDead = false;
    public bool isLoading = false;
    public float health = 0f;
    public HealthBar healthBar;
    public float accuracy = 10;

    public float shootTime = 1f;

	public float damage = 0f;
	public bool hasBraizer = false;

	public Material nightMaterial;
	void Start () 
    {
        animator = GetComponent<Animator>();
        healthBar.InitHealth(health);
        PointsCounter.Instance.UnitsCount++;
        StartCoroutine(Loading());
		if(PointsCounter.Instance.nightMode)
		{
			renderer.material = nightMaterial;
		}
	}
	
    private IEnumerator Loading()
    {
        isLoading = true;
        if(gameObject.name.StartsWith("Crossbowman"))
            SoundManager.Instance.Play("cbowwind");
        yield return new WaitForSeconds(shootTime);
        SoundManager.Instance.Stop("cbowwind");

        isLoading = false; 
    }

    public void Damage(float damage)
    {
        healthBar.UpdateHealth(damage);
        float newHealth = health - damage;
        if (newHealth <= 0f)
        {
            Kill("arrow");
        }
        else
        {
            health = newHealth;
            SoundManager.Instance.PlayRandomSound("hit", 1, 5);
        }
    }
    public IEnumerator Shoot(float angle)
    {
        isLoading = true;
        animator.SetTrigger("Shoot");
        float random = Random.Range(-accuracy, accuracy);
        angle += random;
		GameObject go = null;
		if(hasBraizer)
		{
			go = Instantiate(arrows[1], transform.position, Quaternion.Euler(0, 0, angle)) as GameObject;
		}
		else
		{
        	go = Instantiate(arrows[0], transform.position, Quaternion.Euler(0, 0, angle)) as GameObject;
		}
		go.GetComponent<Projectile>().damage += damage;
        PointsCounter.Instance.arrowsShooted++;
        if(gameObject.name.StartsWith("Crossbowman"))
            SoundManager.Instance.Play("cbowwind");
        yield return new WaitForSeconds(shootTime);
        SoundManager.Instance.Stop("cbowwind");
        isLoading = false;
    }
    public void Kill(string death)
    {
        health = 0f;
        healthBar.UpdateHealth(health);

        PointsCounter.Instance.UnitsCount--;
        isDead = true;
        animator.SetTrigger("Die");
        SoundManager.Instance.PlayRandomSound(death + "_death", 1, 5);
        StartCoroutine(Kill());
    }
    private IEnumerator Kill()
    {
        yield return new WaitForSeconds(1.2f);
        transform.position = new Vector3(0, -50, 0);
        yield return new WaitForSeconds(0.1f);
        Destroy(this.gameObject);
    }

}
