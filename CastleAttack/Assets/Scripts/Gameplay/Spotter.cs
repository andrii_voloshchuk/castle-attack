﻿using UnityEngine;
using System.Collections;

public class Spotter : MonoBehaviour 
{
    public Enemy enemy;

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.tag == "Player")
        {
            enemy.Spoted();
        }
        else if (other.gameObject.name == "GOTrigger")
        {
            if (enemy.enemyType == Enemy.EnemyType.Archer)
            {
                enemy.idle = true;
            }
        }
    }
    void OnTriggerExit2D(Collider2D other)
    {
        if (enemy.enemyType == Enemy.EnemyType.Archer)
        {
            if (other.gameObject.tag == "Player")
            {
                enemy.enemyFound = false;
                enemy.animator.SetBool("Shoot", false);
            }
        }
    }
}
