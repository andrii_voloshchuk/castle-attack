﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HealthBar : MonoBehaviour 
{
    public List<Sprite> bars;
    private SpriteRenderer spriteRenderer;

    private float health = 0f;
    private float initHealth = 0f;

    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }
    public void InitHealth(float health)
    {
        initHealth = health;
        this.health = health;
    }
    public void UpdateHealth(float damage)
    {
        float newHealth = health - damage;

        if(newHealth <= 0f)
        {
            health = 0f;
            spriteRenderer.sprite = bars[0];
        }
        else
        {
            health = newHealth;
            float percentPerHealth = initHealth / 100f;
            float healthInPercent = newHealth / percentPerHealth;
            if (healthInPercent > 1f && healthInPercent < 10f)
            {
                spriteRenderer.sprite = bars[0];
            }
            else if (healthInPercent >= 10f && healthInPercent < 20f)
            {
                spriteRenderer.sprite = bars[1];
            }
            else if (healthInPercent >= 20f && healthInPercent < 30f)
            {
                spriteRenderer.sprite = bars[2];
            }
            else if (healthInPercent >= 30f && healthInPercent < 40f)
            {
                spriteRenderer.sprite = bars[3];
            }
            else if (healthInPercent >= 40f && healthInPercent < 50f)
            {
                spriteRenderer.sprite = bars[4];
            }
            else if (healthInPercent >= 50f && healthInPercent < 60f)
            {
                spriteRenderer.sprite = bars[5];
            }
            else if (healthInPercent >= 60f && healthInPercent < 70f)
            {
                spriteRenderer.sprite = bars[6];
            }
            else if (healthInPercent >= 70f && healthInPercent < 80f)
            {
                spriteRenderer.sprite = bars[7];
            }
            else if (healthInPercent >= 80f && healthInPercent < 90f)
            {
                spriteRenderer.sprite = bars[8];
            }
            else if (healthInPercent >= 90f && healthInPercent < 100f)
            {
                spriteRenderer.sprite = bars[9];
            }
            else if (healthInPercent >= 100f)
            {
                spriteRenderer.sprite = bars[10];
            }
           
        }

    }

}
