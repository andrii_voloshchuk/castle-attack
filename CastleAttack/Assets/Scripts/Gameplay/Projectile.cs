﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour 
{
    public float speed = 0f;

    private bool isHit = false;

    public float damage = 0f;
	public Material nightMaterial;
    public bool isEnemyProjectile = false;

	void Start () 
    {
		if(PointsCounter.Instance.nightMode)
		{
			renderer.material = nightMaterial;
            if(!isEnemyProjectile)
			    transform.GetChild(0).gameObject.SetActive(true);
		}
        Destroy(this.gameObject, 4f);
	}


	void Update () 
    {
        if (!isHit)
        {
            transform.Translate(Vector3.right * speed * Time.deltaTime);
            transform.right = Vector3.Slerp(transform.right, rigidbody2D.velocity.normalized, Time.deltaTime);
        }
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.name == "Grass")
        {
            rigidbody2D.isKinematic = true;
            collider2D.enabled = false;
            isHit = true;
            SoundManager.Instance.PlayRandomSound("arrow_grass", 1, 2);           
        }
        if (!isEnemyProjectile)
        {
            if (other.gameObject.tag == "Enemy")
            {
                if (!other.GetComponent<Enemy>().isDead)
                {
                    PointsCounter.Instance.arrowsHit++;
                    other.GetComponent<Enemy>().Damage(damage);
                    Destroy(this.gameObject);
                }
            }
        }
        else
        {
            if (other.gameObject.tag == "Player")
            {
                if (!other.GetComponent<Archer>().isDead)
                {
                    other.GetComponent<Archer>().Damage(damage);
                    Destroy(this.gameObject);
                }
            }
        }
    }
}
