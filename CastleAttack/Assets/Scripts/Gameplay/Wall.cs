﻿using UnityEngine;
using System.Collections;

public class Wall : MonoBehaviour
{
    public float strength = 1f;
    public Transform ground;

    public Archer archer;

    private bool isDestroyed = false;
	public GameObject braizer;

    void Start()
    {
        Vector2 distance = transform.position - ground.position;
        PointsCounter.Instance.startDefence += Mathf.Abs(distance.y);
        PointsCounter.Instance.currentDefence += Mathf.Abs(distance.y);
    }
	public void SetBraizer(bool value)
	{
		braizer.SetActive(value);
	}
    public void Damage(float damage)
    {
        float newDamage = damage / strength;
        transform.position = new Vector3(transform.position.x, transform.position.y - newDamage, transform.position.z);
        PointsCounter.Instance.currentDefence -= newDamage;
        if(transform.position.y <= ground.position.y && !isDestroyed)
        {
            isDestroyed = true;
			Destroy(braizer.gameObject);
            if(archer)
                archer.Kill("spear");
        }
    }

}
