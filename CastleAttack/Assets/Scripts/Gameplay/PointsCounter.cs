﻿using UnityEngine;
using System.Collections;

public class PointsCounter : MonoBehaviour
{

    private static PointsCounter instance = null;
    public static PointsCounter Instance { get { return instance; } }


    public int arrowsShooted = 0;
    public int arrowsHit = 0;

    public int enemyKills = 0;

    public int enemyKilled = 0;

    private int unitsCount = 0;
    public int UnitsCount
    {
        get
        {
            return unitsCount;
        }
        set
        {
            unitsCount = value;
            if (unitsCount <= 0)
                EventManager.Instance.OnGameOver();
        }
    }
    public float startDefence = 0f;
    public float currentDefence = 0f;

    public bool noHitBonus = false;

    public int accuracy = 0;
    public int survived = 0;
    public int defence = 0;

    public bool nightMode = false;
    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }
        else
            instance = this;
    }
    void Start()
    {
        EventManager.onPointsChanged += EventManager_onPointsChanged;
    }
    void OnDestroy()
    {
        EventManager.onPointsChanged -= EventManager_onPointsChanged;
    }
    void EventManager_onPointsChanged(int points)
    {
        enemyKills += points;
    }
	public int GetTotalPoints()
    {
        int totalPoints = 0;
        totalPoints += enemyKills;

        accuracy = (int)((arrowsHit / (float)arrowsShooted) * 100f);

        totalPoints += accuracy;

        survived = unitsCount * 10;
        totalPoints += survived;

        if(currentDefence < 0f)
        {
            currentDefence = 0f;
        }
        defence = (int)(((currentDefence) / startDefence) * 100f);
        totalPoints += defence;

        if(currentDefence == startDefence)
        {
            noHitBonus = true;
            SocialManager.Instance.ReportAchievement("CggIyuvwll4QAhAC");
            totalPoints += 50;
        }
        SocialManager.Instance.ReportAchievement("CggIyuvwll4QAhAD", enemyKilled);
        SocialManager.Instance.ReportAchievement("CggIyuvwll4QAhAE", enemyKilled);
        SocialManager.Instance.ReportAchievement("CggIyuvwll4QAhAF", enemyKilled);
        SocialManager.Instance.ReportAchievement("CggIyuvwll4QAhAG", enemyKills);
        SocialManager.Instance.ReportAchievement("CggIyuvwll4QAhAH", enemyKills);
        SocialManager.Instance.ReportAchievement("CggIyuvwll4QAhAI", enemyKills);

        if (accuracy >= 30)
        {
            SocialManager.Instance.ReportAchievement("CggIyuvwll4QAhAK");
        }
        if (accuracy >= 50)
        {
            SocialManager.Instance.ReportAchievement("CggIyuvwll4QAhAL");
        }
        if (accuracy >= 70)
        {
            SocialManager.Instance.ReportAchievement("CggIyuvwll4QAhAM");
        }

        return totalPoints;
    }
}
