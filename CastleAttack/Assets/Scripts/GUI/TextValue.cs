﻿using UnityEngine;
using System.Collections;

public class TextValue : MonoBehaviour 
{
    public enum TextType
    {
        Points, Gold
    }
    public TextType textType;
    private TextMesh textMesh;

    private int points = 0;

	void Start ()
    {
        

        textMesh = GetComponent<TextMesh>();
        EventManager.onPointsChanged += EventManager_onPointsChanged;
        if (textType == TextType.Gold)
        {
            textMesh.text = ProgressManager.Instance.gold.ToString();
            points = ProgressManager.Instance.gold;
        }

        
	}
    void OnDestroy()
    {
         EventManager.onPointsChanged -= EventManager_onPointsChanged;
    }
    void EventManager_onPointsChanged(int points)
    {
        this.points += points;
        textMesh.text = this.points.ToString();
    }



}
