﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour 
{
    private string sceneName;

	void Start () 
    {
        sceneName = Application.loadedLevelName;
        SetCameraSize();
	}	
 
    void SetCameraSize()
    {
        float height = Screen.height;
        float width = Screen.width;

        float ratio = width / height;

        if(ratio >= 1.2f && ratio < 1.3f)
        {
            camera.orthographicSize = 7.1f;
            if(sceneName == "World")
                transform.position = new Vector3(transform.position.x, 1.5f, transform.position.z);
        }
        else if (ratio >= 1.3f && ratio < 1.45f)
        {
            camera.orthographicSize = 6.7f;
            if (sceneName == "World")
                transform.position = new Vector3(transform.position.x, 1f, transform.position.z);
        }
        else if (ratio >= 1.45f && ratio < 1.55f)
        {
            camera.orthographicSize = 5.9f;
            if (sceneName == "World")
                transform.position = new Vector3(transform.position.x, 0.5f, transform.position.z);
        }
        else if (ratio >= 1.55f && ratio < 1.65f)
        {
            camera.orthographicSize = 5.5f;
        }
        else if (ratio >= 1.65f && ratio < 1.8f)
        {
            camera.orthographicSize = 5f;
        }
    }
}
