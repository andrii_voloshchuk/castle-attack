﻿using UnityEngine;
using System.Collections;

public class MapCamera : MonoBehaviour 
{
    private float startSize = 0f;
    public SpriteRenderer worldMap;
    private Sprite map;
    float height = Screen.height;
    float width = Screen.width;
    Vector3 lastPosition;
    float ratio = 0f;
	void Start () 
    {
        startSize = camera.orthographicSize;
        map = worldMap.sprite;
        ratio = width / height;
	}
	
	void Update () 
    {
#if UNITY_STANDALONE || UNITY_EDITOR
            camera.orthographicSize -= Input.GetAxis("Mouse ScrollWheel");
            camera.orthographicSize = Mathf.Clamp(camera.orthographicSize, startSize / 2f, startSize);
            transform.position = new Vector3(Mathf.Clamp(transform.position.x, map.bounds.min.x * 2 + camera.orthographicSize * ratio, map.bounds.max.x * 2 - camera.orthographicSize * ratio),
                    Mathf.Clamp(transform.position.y, map.bounds.min.y * 2 + camera.orthographicSize, map.bounds.max.y * 2 - camera.orthographicSize), -10);
         
         if (Input.GetMouseButtonDown(0))
         {
             lastPosition = Input.mousePosition;
         }
 
         if (Input.GetMouseButton(0))
         {
             Vector3  delta  = Input.mousePosition - lastPosition;
             transform.Translate(-delta.x * 0.01f, -delta.y * 0.01f, 0);
             lastPosition = Input.mousePosition;
             transform.position = new Vector3(Mathf.Clamp(transform.position.x, map.bounds.min.x * 2 + camera.orthographicSize * ratio, map.bounds.max.x * 2 - camera.orthographicSize * ratio),
                Mathf.Clamp(transform.position.y, map.bounds.min.y * 2 + camera.orthographicSize, map.bounds.max.y * 2 - camera.orthographicSize), -10);
         }
#elif UNITY_ANDROID || UNITY_IOS || UNITY_WP8 
        if (Input.touchCount == 2)
        {
            Touch touchZero = Input.GetTouch(0);
            Touch touchOne = Input.GetTouch(1);

            Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
            Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

            float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
            float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

            float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;

            camera.orthographicSize += deltaMagnitudeDiff * 0.1f;
            camera.orthographicSize = Mathf.Clamp(camera.orthographicSize, startSize / 2f, startSize);
            transform.position = new Vector3(Mathf.Clamp(transform.position.x, map.bounds.min.x * 2 + camera.orthographicSize * ratio, map.bounds.max.x * 2 - camera.orthographicSize * ratio),
                Mathf.Clamp(transform.position.y, map.bounds.min.y * 2 + camera.orthographicSize, map.bounds.max.y * 2 - camera.orthographicSize), -10);
        }
        else if (Input.touchCount == 1 &&  Input.GetTouch(0).phase == TouchPhase.Moved)
        {
//            Vector2 touchDeltaPosition = Input.GetTouch(0).deltaPosition;
            transform.Translate(FixTouchDelta(Input.GetTouch(0)));
            transform.position = new Vector3(Mathf.Clamp(transform.position.x, map.bounds.min.x * 2 + camera.orthographicSize * ratio, map.bounds.max.x * 2 - camera.orthographicSize * ratio),
                Mathf.Clamp(transform.position.y, map.bounds.min.y * 2 + camera.orthographicSize, map.bounds.max.y * 2 - camera.orthographicSize), -10);
        }

#endif
    }
    private Vector3  FixTouchDelta(Touch aT)
    {
        float dt = Time.deltaTime / aT.deltaTime;
        if (float.IsNaN(dt) || float.IsInfinity(dt))
            dt = 1.0f;
        return new Vector3( - (aT.deltaPosition.x * dt * 3) / Screen.dpi,  - (aT.deltaPosition.y * dt * 3) / Screen.dpi, -10)  ;
    }
}
