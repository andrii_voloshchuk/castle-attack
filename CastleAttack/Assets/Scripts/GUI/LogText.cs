﻿using UnityEngine;
using System.Collections;

public class LogText : MonoBehaviour 
{
    private TextMesh textMesh;

	void Start () 
    {
        textMesh = GetComponent<TextMesh>();
        Destroy(this.gameObject, 3f);
	}
	

	void Update () 
    {
        transform.Translate(Vector3.up * Time.deltaTime );
        textMesh.color = new Color(1, 1, 1, textMesh.color.a - (Time.deltaTime/2.5f));
	}
}
