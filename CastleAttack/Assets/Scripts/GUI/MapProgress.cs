﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MapProgress : MonoBehaviour 
{
    public List<GameObject> levels = new List<GameObject>();

    public Transform avatar;
    private List<Level> p_levels = new List<Level>();

	void Start ()
    {
        SoundManager.Instance.Stop("victory");
        MusicManager.Instance.PlayOneClip("MainMenu");

        p_levels = ProgressManager.Instance.levels;
        for (int i = 0; i < levels.Count; i++)
        {
            if (p_levels[i].state == 0)
            {
                levels[i].SetActive(false);
            }
            else if (p_levels[i].state == 1)
            {
                levels[i].transform.GetChild(0).gameObject.SetActive(false);
                avatar.position = levels[i].transform.position;
            }
            else if (p_levels[i].state == 2 && i == levels.Count - 1)
            {
                avatar.position = levels[i].transform.position;
            }

        }
        ReportScores();
	}
    void ReportScores()
    {
        int sum = 0;
        for (int i = 0; i < p_levels.Count; i++)
        {
            sum += p_levels[i].score;
        }
        if (sum > 0)
        {
            SocialManager.Instance.ReportScore(sum);
        }
    }
	
}
