﻿using UnityEngine;
using System.Collections;

public class Loading : MonoBehaviour 
{
    public Transform progress;
    private AsyncOperation loading = null;
    private Animator animator;

    private float sizeY, sizeZ = 0f;

    public IEnumerator LoadScene(string name, bool first)
    {
        if (!first)
        {
            animator.SetTrigger("Start");
            yield return new WaitForSeconds(0.5f);
        }
        loading = Application.LoadLevelAsync(name);
        yield return loading;
    }
	void Start () 
    {
        animator = GetComponent<Animator>();
        sizeY = progress.localScale.y;
        sizeZ = progress.localScale.z;
	}
	
    private IEnumerator Finish()
    {
        yield return new WaitForSeconds(1f);
        loading = null;
        animator.SetTrigger("Finish");
        yield return new WaitForSeconds(0.5f);
        progress.localScale = new Vector3(0f, sizeY, sizeZ);
    }
	void Update () 
    {

	    if(loading != null)
        {
            if (!loading.isDone)
            {
                progress.localScale = new Vector3(loading.progress * 100, sizeY, sizeZ);
            }
            else
            {
                progress.localScale = new Vector3(100, sizeY, sizeZ);
                StartCoroutine(Finish());
            }
        }
        
	}
}
