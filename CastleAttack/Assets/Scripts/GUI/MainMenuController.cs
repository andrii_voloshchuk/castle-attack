﻿using UnityEngine;
using System.Collections;

public class MainMenuController : MonoBehaviour 
{
    public GameObject[] uis;
    private static MainMenuController instance;
    public static MainMenuController Instance { get { return instance; } }

    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }
        else
            instance = this;
    }
    void OnDestroy()
    {
        instance = null;
    }
    public void SetUiVisible(int ind)
    {
        for (int i = 0; i < uis.Length; i++)
        {
            if(i == ind)
                uis[i].SetActive(true);
            else
                uis[i].SetActive(false);
        }
    }
}
