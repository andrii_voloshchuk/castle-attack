﻿using UnityEngine;
using System.Collections;

public class Statistic : MonoBehaviour
{
    private bool startMove = false;
    private float percent = 0f;

    public void SetStatstic(float current, float max)
    {
        if (current <= max)
        {
            percent = (current / max) * 100f;
            startMove = true;
        }
    }
    public void Reset()
    {
        startMove = false;
        transform.localScale = new Vector3(0, transform.localScale.y, 1);
    }
	void Start () 
    {

	}
	

	void Update () 
    {
	    if(startMove)
        {
            if(transform.localScale.x < percent)
            {
                transform.localScale = new Vector3(transform.localScale.x + Time.deltaTime * 50, 1, 1);
            }
            else
            {
                startMove = false;
            }
        }
	}
}
