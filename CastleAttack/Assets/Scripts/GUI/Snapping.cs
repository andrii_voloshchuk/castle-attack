﻿using UnityEngine;
using System.Collections;

public class Snapping : MonoBehaviour 
{
    public enum Snap
    {
        RightDown, LeftDown, RightUp, LeftUp, Top, Bottom
    }
    public enum SnapCamera
    {
        Main, GUI
    }
    public Snap snap;
    public SnapCamera snapCamera;
    private Camera cam;


    private SpriteRenderer spriteRenderer;

	void Start () 
    {
        if (snapCamera == SnapCamera.Main)
            cam = Camera.main;
        else
            cam = GameObject.FindGameObjectWithTag("GUICamera").GetComponent<Camera>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        Vector3 screen = cam.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0));
        Vector3 zero = cam.ScreenToWorldPoint(new Vector3(0, 0, 0));
	    switch(snap)
        {
            case Snap.RightDown:
                transform.position = new Vector3(screen.x - spriteRenderer.sprite.bounds.size.x, zero.y + spriteRenderer.sprite.bounds.size.y, transform.position.z);
                break;     
            case Snap.LeftDown:
                transform.position = new Vector3(zero.x + spriteRenderer.sprite.bounds.size.x, zero.y + spriteRenderer.sprite.bounds.size.y, transform.position.z);
                break;
            case Snap.Top:
                transform.position = new Vector3(transform.position.x, screen.y - spriteRenderer.sprite.bounds.size.y , transform.position.z);
                break;
            case Snap.Bottom:
                transform.position = new Vector3(transform.position.x, zero.y + spriteRenderer.sprite.bounds.size.y, transform.position.z);
                break;

        }
	}

	
}
