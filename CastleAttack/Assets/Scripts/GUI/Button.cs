﻿using UnityEngine;
using System.Collections;

public class Button : MonoBehaviour 
{

    public enum EventName
    {
        None, ToMap, PlayLevel, ToMenu,
        ArcherLeft, ArcherRight, ToShop, 
        CategoryLeft, CategoryRight,
        Improve, Buy, LeaderBoard, 
        Options, Achievements, About, 
        Sound, Music, Back, Pause
    }

    public EventName eventName;

    public Sprite normal;
    public Sprite pressed;

    private SpriteRenderer spriteRenderer;

    public bool noSprite = false;

    private string buttonName = "";
    public bool switchButton = false;

    TextMesh textMesh;

    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        buttonName = this.gameObject.name;
        if (switchButton)
        {
            textMesh = transform.GetChild(0).GetComponent<TextMesh>();

            if (buttonName.StartsWith("Sound"))
            {
                if (PlayerPrefs.GetString("sound") == "False")
                {
                    textMesh.text = "Off";
                }
            }
            else
            {
                if (PlayerPrefs.GetString("music") == "False")
                {
                    textMesh.text = "Off";
                }
            }

        }
    }

    void OnMouseDown()
    {
        SoundManager.Instance.PlayOneSound("click");
        if(!noSprite)
            spriteRenderer.sprite = pressed;
      
    }
    void OnMouseUp()
    {
        if(!noSprite)
            spriteRenderer.sprite = normal;
        Global.Instance.ButtonEvent(eventName, buttonName);
        if (switchButton)
        {
            if (textMesh.text == "On")
                textMesh.text = "Off";
            else
                textMesh.text = "On";
        }
    }

}
